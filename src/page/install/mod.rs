/* SPDX-License-Identifier: GPL-3.0-or-later */

mod done_page;
mod page;

pub use page::InstallPage;
